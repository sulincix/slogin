using Gtk;
using Posix;
extern int authenticate_system(string username,string pass);
int main(string[] args){
	Gtk.init(ref args);
	var w=new Gtk.Window();
	w.deletable=false;
	w.window_position = WindowPosition.CENTER;
	var user=new Gtk.Entry();
	var passwd=new Gtk.Entry();
	passwd.set_invisible_char('*');
	passwd.set_visibility(false);
	Gtk.Box box = new Gtk.Box (Gtk.Orientation.VERTICAL, 5);
	w.add(box);
	box.pack_start(user,true,true,5);
	box.pack_start(passwd,true,true,5);
	var but = new Gtk.Button();
	box.pack_start(but,true,true,5);
	Posix.system("bash /usr/lib/sloginwm/sloginwmrc");
	w.show_all();
	w.set_opacity(0.8);
	but.clicked.connect(()=>{
		string username=user.get_text();
		string pass=passwd.get_text();
		string session="startxfce4";
		int i=authenticate_system(username,pass);
		user.set_text(i.to_string());
		if(i==1){
			Posix.system("echo USER="+username+"> /tmp/sloginwm");
			Posix.system("echo SESSION="+session+">> /tmp/sloginwm");
			Gtk.main_quit();
		}else{
			user.set_text("");
			passwd.set_text("");
			passwd.set_invisible_char('*');
		}
	});
	Gtk.main();
	return 0;	
}
