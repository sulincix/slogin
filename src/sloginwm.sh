#!/bin/bash
while true
do
	rm -f /tmp/sloginwm
	su slogin -c "xinit /usr/lib/sloginwm/graphic -- /etc/X11/xinit/xserverrc"
	if [ "$?" == "0" ]
	then
		. /tmp/sloginwm
		[ "${USER}" != "" ] || exit 1
		su "${USER}" -c "cd ~ ; SESSION=${SESSION} sxinit"
	fi
done
}
