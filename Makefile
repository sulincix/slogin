
all: clean build

build:
	mkdir build || true
	valac src/checker.c src/graphic.vala  --pkg gtk+-3.0 --pkg posix --Xcc=-lpam -o build/graphic
clean:
	rm -rf build
install:
	mkdir -p /usr/lib/sloginwm || true
	mkdir -p /usr/bin/ || true
	install build/graphic /usr/lib/sloginwm/graphic
	install src/sloginwmrc /usr/lib/sloginwm/sloginwmrc
	install src/background.png /usr/lib/sloginwm/background.png
	install src/sloginwm.sh /usr/bin/sloginwm
